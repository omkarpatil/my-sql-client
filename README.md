**My Sql Client**

Basic sql client written in tuby.

---

## Installation

*Note: Make sure rvm is installed on your machine.*

1. Download or clone the repository.
2. cd to **mysql_client** directory.
3. It will show message that gemset is create created. If message in not displayed then install ruby by using **rvm install ruby-2.4.4**
3. Now type command **"cd ."** (Because rvm overrides ubuntu cd command).
4. run **gem install bundler**.
5. run **bundle install**.
6. create file named **.env** under projects root directory(i.e. **mysql_client**).
7. Paste configuration of remote server.
8. Run **ruby run.rb**
