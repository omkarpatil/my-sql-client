require_relative 'base'
require 'mysql2'
module Database
  module Adapter
    class MySqlAdapter < Base
      attr_accessor :options

      extend Forwardable

      def initialize(options = {})
        @options = self.class.parsed_config_options.merge(options)
      end

      def client
        @client ||= ::Mysql2::Client.new(@options)
      end

      def execute(query)
        client.query(query)
      end

      def print_result(results)
        print_array(results.fields)
        puts ""
        results.each { |row| print_array(row.values); puts "" } 
        
      end

      def method_missing(m, *args, &block)
        client.send(m, *args, &block)
      end

      def respond_to?(method_name, include_private = false)
        self.respond_to?(method_name, include_private) || client.respond_to?(method_name, include_private)
      end

      class << self
        def default_options
          {
            'host' => 'localhost',
            'username' => 'root',
            'password' => '',
            'port' => 3306
          }
        end

        def fetch_options_from_config
          Database::Core.fetch_options(Application.config.database_config_path)
        end

        def parsed_config_options
          default_options.merge(self.fetch_options_from_config)
        end
      end

      private

        def print_array(values)
          values.each do |v|
            print v.to_s.ljust(15)
          end
        end

    end
  end
end
