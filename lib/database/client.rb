module Database
  class Client
    extend Forwardable
    attr_accessor :adapter

    def_delegator :adapter, :execute

    # Use MySqlAdapter as default adapter
    def initialize(adapter: Database::Adapter::MySqlAdapter.new)
      @adapter = adapter
    end

    def print_result(results)
      adapter.print_result(results)
    end

  end
end

