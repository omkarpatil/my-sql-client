require 'yaml'
require 'erb'
module Database
  class Core
    class << self
      def fetch_options(path)
        yaml_contents = File.open(path).read
        options = YAML.load( ERB.new(yaml_contents).result )
      end
    end
  end
end
