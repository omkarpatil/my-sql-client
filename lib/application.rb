require_relative 'support/configurable'
require_relative 'database'

class Application
  include Support::Configurable

  config.root = Application.find_root
  config.database_config_path =  config.root + '/config/database.yml'

end
