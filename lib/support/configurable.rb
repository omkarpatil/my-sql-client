require 'ostruct'
require 'pry'

module Support
  module Configurable

    def self.included(klass)
      klass.extend(ClassMethods)
    end

    module ClassMethods
      def config
        @_config ||= OpenStruct.new
      end

      def configure
        yield config
      end

      def find_root
        Dir.pwd
      end
    end

  end
end
