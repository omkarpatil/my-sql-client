require 'boot'

RSpec.describe Database::Core do
  describe ".fetch_options" do
    context 'given proper file path' do

      let(:path) { Application.config.root + '/spec/test_files/test_database.yml' }
      let(:required_output) do
        {
          "host"=>"test_host", "username"=>"test_username",
          "password"=>"test_password", "database"=>"test_database"
        }
      end

      it 'returns contents in hash' do
        result = Database::Core.fetch_options(path)
        expect(result).to be_a_kind_of(Hash)
      end

      it 'returns proper output' do
        result = Database::Core.fetch_options(path)
        expect(result).to be_eql(required_output)
      end

    end

  end
end
