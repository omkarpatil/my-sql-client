require 'boot'

RSpec.describe Database::Client do

  describe '#adapter' do
    context 'when adapter is not given' do
      it 'uses MySqlAdapter as default adapter' do
        expect(subject.adapter).to be_an_instance_of(Database::Adapter::MySqlAdapter)
      end
    end
  end

end
