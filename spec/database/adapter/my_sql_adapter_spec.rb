require 'boot'

RSpec.describe Database::Adapter::MySqlAdapter do

  describe '#client' do
    it 'returns MySql::Client instance' do
      expect(subject.client).to be_an_instance_of(Mysql2::Client)
    end

    it 'returns same object every time' do
      expect(subject.client).to be(subject.client)
    end
  end

  describe '.default_options' do
    let(:expected_options) do
      {
        'host' => 'localhost',
        'username' => 'root',
        'password' => '',
        'port' => 3306
      }
    end

    it 'retuns default_options' do
      expect(subject.class.default_options).to be_eql(expected_options)
    end
  end
end
