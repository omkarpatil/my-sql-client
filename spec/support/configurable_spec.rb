require 'boot'

RSpec.describe Support::Configurable do
  include Support::Configurable

  describe "Class Methods" do
    describe 'config'

    context 'config' do

      it 'returns OpenStruct instance' do
        result = RSpec::ExampleGroups::SupportConfigurable.config
        expect(result).to be_an_instance_of(OpenStruct)
      end

    end

  end
end
