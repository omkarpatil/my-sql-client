require_relative 'lib/boot'

client = Database::Client.new
adapter = client.adapter
options = adapter.options

puts "Welcome to MySql Client"
puts "You are connected using following options"
options.each do |key, value|
  puts "#{key}  :  #{value}"
end
puts "type exit to exit from client"
puts "-------------------------------------------------------------------------"
query = nil

loop do
  begin
    print '> '
    query = gets.chomp
    break if query.eql?('exit')
    results = client.execute(query)
    client.print_result(results)
  rescue Exception => e
    puts e.message
  end
end

puts "Bye!"
